package com.eagleeye.userservice.converter;

import java.util.List;

public interface IListConverter<E, D> {
    List<E> convertToEntity(List<D> dto);
    List<D> convertToDto(List<E> entity);
}
