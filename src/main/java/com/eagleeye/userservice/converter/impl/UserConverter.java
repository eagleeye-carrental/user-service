package com.eagleeye.userservice.converter.impl;

import com.eagleeye.userservice.converter.IConverter;
import com.eagleeye.userservice.converter.IListConverter;
import com.eagleeye.userservice.document.User;
import commonentity.user.UserDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserConverter implements IConverter<User, UserDto>, IListConverter<User, UserDto> {
    private final ModelMapper modelMapper;

    @Autowired
    public UserConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public User convertToEntity(UserDto userDto) {
        User user = null;
        if (userDto != null) {
            user = new User();
            modelMapper.map(userDto, user);
        }
        return user;
    }

    @Override
    public UserDto convertToDto(User entity) {
        UserDto userDto = null;
        if (entity !=null) {
            userDto = new UserDto();
            modelMapper.map(entity, userDto);
        }
        return userDto;
    }

    @Override
    public List<User> convertToEntity(List<UserDto> dto) {
        List<User> userList = null;
        return null;
    }

    @Override
    public List<UserDto> convertToDto(List<User> entity) {
        return null;
    }
}
