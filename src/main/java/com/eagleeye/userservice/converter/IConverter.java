package com.eagleeye.userservice.converter;

public interface IConverter<E, D> {
    public E convertToEntity(D dto);
    public D convertToDto(E entity);
}
