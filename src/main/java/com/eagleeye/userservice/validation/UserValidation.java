package com.eagleeye.userservice.validation;

import com.eagleeye.userservice.errors.UserErrorDto;
import com.eagleeye.userservice.services.UserService;
import commonentity.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserValidation {
    private final UserService userService;
    private UserErrorDto userError;

    @Autowired
    public UserValidation(UserService userService) {
        this.userService = userService;
    }

    public UserErrorDto customerRegisterValidation(UserDto userDto, String action) {
        boolean valid = true;
        userError = new UserErrorDto();

        if (action.equalsIgnoreCase("update")) {
            valid = checkFirstName(userDto) && valid;
            valid = checkLastName(userDto) && valid;
            valid = checkEmail(userDto) && valid;
            valid = checkAddress1(userDto) && valid;
            valid = checkPostCode(userDto) && valid;
            valid = checkCountry(userDto) && valid;
            valid = checkEmail(userDto) && valid;
            valid = checkDrivingLicenseNumber(userDto) && valid;
            valid = checkDob(userDto) && valid;
            userError.setValid(valid);
            return userError;
        } else if (action.equalsIgnoreCase("create")) {
            userError.setValid(valid);
            return userError;
        } else {
            userError.setValid(false);
            return userError;
        }


    }

    private boolean checkDob(UserDto userDto) {
        return true;
    }

    private boolean checkDrivingLicenseNumber(UserDto userDto) {
        if (userDto.getDrivingLicenseNumber() == null
                ||
                userDto.getDrivingLicenseNumber().isEmpty()) {
            userError.setDrivingLicenseNumber("Driving License Number Required");
            return false;
        }
        return true;
    }

    private boolean checkCountry(UserDto userDto) {
        if (userDto.getCountry() == null
                ||
                userDto.getCountry().isEmpty()) {
            userError.setCountry("Please,Select Country");
            return false;
        }
        return true;
    }

    private boolean checkPostCode(UserDto userDto) {
        if (userDto.getPostcode() == null
                ||
                userDto.getPostcode().isEmpty()) {
            userError.setPostcode("Postcode Required!");
            return false;
        }
        return true;
    }

    private boolean checkAddress1(UserDto userDto) {
        if (userDto.getAddress1() == null
                ||
                userDto.getAddress1().isEmpty()) {
            userError.setAddress1("Address Required!");
            return false;
        }
        return true;
    }

    private boolean checkEmail(UserDto userDto) {
        if (userDto.getEmail() == null
                ||
                userDto.getEmail().isEmpty()) {
            userError.setEmail("Email required!");
            return false;
        }
        return true;
    }

    private boolean checkLastName(UserDto userDto) {
        if (userDto.getLastName() == null
                ||
                userDto.getLastName().isEmpty()) {
            userError.setLastName("Last Name required!");
            return false;
        }
        return true;
    }

    private boolean checkFirstName(UserDto userDto) {
        if (userDto.getFirstName() == null
                ||
                userDto.getFirstName().isEmpty()) {
            userError.setFirstName("First Name required!");
            return false;
        }
        return true;
    }
}
