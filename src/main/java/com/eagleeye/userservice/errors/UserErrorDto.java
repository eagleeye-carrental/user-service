package com.eagleeye.userservice.errors;

import commonentity.user.UserDto;
import lombok.Data;

import java.time.LocalDate;
@Data
public class UserErrorDto {
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String address3;
    private String state;
    private String postcode;
    private String country;
    private String email;
    private String phoneNumber;
    private String mobileNumber;
    private String drivingLicenseNumber;
    private LocalDate dob;
    private boolean valid;
}
