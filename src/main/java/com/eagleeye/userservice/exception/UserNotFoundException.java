package com.eagleeye.userservice.exception;

import org.springframework.stereotype.Component;

public class UserNotFoundException extends Exception{
    public UserNotFoundException(String message) {
        super(message);
    }
}
