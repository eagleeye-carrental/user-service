package com.eagleeye.userservice.controller;

import com.eagleeye.userservice.errors.UserErrorDto;
import com.eagleeye.userservice.responseDto.ResponseDto;
import com.eagleeye.userservice.services.UserService;
import com.eagleeye.userservice.validation.UserValidation;
import commonentity.user.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;
    private final UserValidation userValidation;

    @Autowired
    public UserController(UserService userService, UserValidation userValidation) {
        this.userService = userService;
        this.userValidation = userValidation;
    }

    /*
    * Request param can be create or update
    * create param:: ignore validation for user registration
    * update param:: validation are mandatory
    * */
    @PostMapping("/register")
    public ResponseEntity<ResponseDto> register(@RequestBody UserDto userDto,
                                                @RequestParam(value = "action", defaultValue = "create") String action) {
        ResponseDto responseDto;
        try {
            UserErrorDto validation = userValidation.customerRegisterValidation(userDto, action);
            if (validation.isValid()) {
                Optional<UserDto> user = userService.createUser(userDto);
                logger.info("New User Created Successfully with username %s", user.get().getEmail());
                responseDto = new ResponseDto("User Created Successfully", user);
                URI uri = MvcUriComponentsBuilder.fromController(getClass()).path("/{id}")
                        .buildAndExpand(user.get().getId()).toUri();
                return ResponseEntity.created(uri).body(responseDto);
            } else {
                responseDto = new ResponseDto("Validation Error!", validation);
                return new ResponseEntity<>(responseDto, HttpStatus.CONFLICT);
            }

        } catch (Exception e) {
            logger.debug("Internal Server Error %s", e.fillInStackTrace());
            responseDto = new ResponseDto("Exception", null);
            return new ResponseEntity<>(responseDto, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto> UserDto(@PathVariable("id") String id) {
        Optional<UserDto> user = userService.user(id);
        ResponseDto response;
        if (user.isPresent()) {
            response = new ResponseDto("user found", user);
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.ok(new ResponseDto("can not find user with id " + id, new UserDto()));
    }

    @GetMapping("/list")
    public ResponseEntity<ResponseDto> UserDtos() {
        ResponseDto responseDto;
        List<UserDto> userList = userService.users();
        if (userList.size() > 0) {
            return ResponseEntity.ok(new ResponseDto("user_list", userList));
        }
        return ResponseEntity.ok(new ResponseDto("no Data found!", new ResponseDto()));
    }

    @PutMapping("/{id}/edit")
    public ResponseEntity<UserDto> updateUserDto(@PathVariable("id") String id,
                                                 @RequestBody UserDto existingUserDto) {
        return null;
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<ResponseDto> deleteUser(@PathVariable("id") String id) {
        return null;
    }
}
