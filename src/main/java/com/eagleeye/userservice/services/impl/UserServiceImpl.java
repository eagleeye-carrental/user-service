package com.eagleeye.userservice.services.impl;

import com.eagleeye.userservice.converter.impl.UserConverter;
import com.eagleeye.userservice.document.User;
import com.eagleeye.userservice.repository.UserRepository;
import com.eagleeye.userservice.services.UserService;
import commonentity.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserConverter userConverter;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }

    @Override
    public Optional<UserDto> createUser(UserDto user) {
        if (getUserById(user.getId()).isPresent()
                ||
                getUserByEmail(user.getEmail()).isPresent()
                ||
                getUserByMobileNumber(user.getMobileNumber()).isPresent()
        ) {
            return updateUser(user.getId(), user);
        } else {
            User newUser = userRepository.save(userConverter.convertToEntity(user));
            return Optional.of(userConverter.convertToDto(newUser));
        }
    }

    @Override
    public Optional<UserDto> user(String userId) {
        Optional<User> user = getUserById(userId);
        if (user.isPresent()) {
            return Optional.of(userConverter.convertToDto(user.get()));
        }
        return Optional.empty();
    }

    @Override
    public List<UserDto> users() {
        return userRepository.findAll()
                .stream()
                .map(userConverter::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<UserDto> updateUser(String userId, UserDto newDetails) {
        return Optional.empty();
    }

    @Override
    public String deleteUser(String userId) {
        return null;
    }

    private Optional<User> getUserById(String userId) {
        return userRepository.findById(userId);
    }

    private Optional<User> getUserByMobileNumber(String mobileNumber) {
        return userRepository.findByMobileNumber(mobileNumber);
    }

    private Optional<User> getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
