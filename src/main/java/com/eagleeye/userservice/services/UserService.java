package com.eagleeye.userservice.services;

import commonentity.user.UserDto;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<UserDto> createUser(UserDto user);
    Optional<UserDto> user(String userId);
    List<UserDto> users();
    Optional<UserDto> updateUser(String userId, UserDto existingUser);
    String deleteUser(String userId);
}
